#include <iostream>
using namespace std;
class Class
{
protected:
	int a, b;
	int pole, obwod;
	string kolorek, opis;
public:
	Class() {};
	Class(int A, int B, string KOLOR, string OPIS) { a = A; b = B; kolorek = KOLOR; opis = OPIS; };
	void liczPole(int a, int b) { int pole = a * b; };
	void liczObwod(int a, int b) { int obwod = 2 * (a + b); };
	void podajKolor(string kolorek) { cout << kolorek; };
	void cos(string opis) { cout << opis; };
};